﻿namespace UnityWebRTC.Utilities
{
    /// <summary>
    /// Enumerable representing video capture resolution.
    /// </summary>
    public enum CapRes
    {
        Default = 0,
        _640_480 = 1,
        _320_240 = 2,
    };
    
    /// <summary>
    /// Enumerable representing video frames per second.
    /// </summary>
    public enum CapFPS
    {
        Default = 0,
        _5 = 1,
        _15 = 2,
        _30 = 3
    };
}
