﻿/***********************************************************
 * 
 * Based on samples from https://github.com/webrtc-uwp
 * 
 **********************************************************/

using System.Xml.Serialization;

namespace UnityWebRTC.Utilities
{
    /// <summary>
    /// A base class for validable values.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    public abstract class ValidableBase<T> : BindableBase
    {
        private T _value;

        /// <summary>
        /// The value to validate.
        /// </summary>
        public T Value
        {
            get { return _value; }
            set
            {
                if (SetProperty(ref _value, value))
                {
                    Validate();
                }
            }
        }

        [XmlIgnore] // Not sure, if neccessary for a unity-project
        bool _valid = true;
       
        /// <summary>
        /// Property to indicate if the value is valid.
        /// </summary>
        [XmlIgnore] // Not sure, if neccessary for a unity-project
        public bool Valid
        {
            get { return _valid; }
            protected set { SetProperty(ref _valid, value); }
        }

        /// <summary>
        /// Validate that the value meets the requirements for the
        /// specific validable classes.
        /// </summary>
        abstract protected void Validate();
    }
}
