﻿using System;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Linq;
using UnityWebRTC.Interface;

#if NETFX_CORE
using Windows.Media.Effects;
using Windows.Foundation.Collections;
using Windows.Media.Capture;
using Windows.Storage.Streams;
using Windows.Media.MediaProperties;
using Windows.UI.Popups;
using System.Threading.Tasks;
using UnityWebRTC.Signalling;
using UnityWebRTC.Model;
using Windows.UI.Core;
using webrtc_winrt_api;
#endif

namespace UnityWebRTC
{
    /// <summary>
    /// Implements WebRTC for Unity to develop Universal-Windows-Apps
    /// </summary>
    public class UWebRTC
    {
        public event Action OnPeerConnectionCreated;
        public event Action OnPeerConnectionClosed;
        public event Action OnReadyToConnect;
        public event Action OnError;

        #region Variables
        // Vaiablen
#if NETFX_CORE
        #region Variables-Unity
        /// <summary>
        /// Static Dispatcher, must be manually set in app.xaml.cs
        /// </summary>
        public static CoreDispatcher Dispatcher
        {
            set { _dispatcher = value; }
            get { return _dispatcher; }
        }

        private static CoreDispatcher _dispatcher = null;

        private Collection<String> _allCapRes;

        /// <summary>
        /// The list of all capture resolutions.
        /// </summary>
        public Collection<String> AllCapRes
        {
            get
            {
                if (_allCapRes == null)
                {
                    _allCapRes = new Collection<String>();
                }
                return _allCapRes;
            }
            set {  _allCapRes = value; }
        }

        private Collection<MediaDevice> _cameras;

        /// <summary>
        /// The list of available cameras.
        /// </summary>
        private Collection<MediaDevice> Cameras
        {
            get { return _cameras; }
            set
            {
                _cameras = value;
            }
        }

        private MediaDevice _selectedCamera;

        /// <summary>
        /// The selected camera.
        /// </summary>
        private MediaDevice SelectedCamera
        {
            get { return _selectedCamera; }
            set
            {
                _selectedCamera = value;

                if (value == null)
                {
                    return;
                }

                Conductor.Instance.Media.SelectVideoDevice(_selectedCamera);
                
                if (_allCapRes == null)
                {
                    _allCapRes = new Collection<String>();
                }
                else
                {
                    _allCapRes.Clear();
                }

                var opRes = value.GetVideoCaptureCapabilities();
                opRes.AsTask().ContinueWith(resolutions =>
                {
                    RunOnUiThread(async () =>
                    {
                        if (resolutions.IsFaulted)
                        {
                            Exception ex = resolutions.Exception;
                            while (ex is AggregateException && ex.InnerException != null)
                                ex = ex.InnerException;
                            String errorMsg = "SetSelectedCamera: Failed to GetVideoCaptureCapabilities (Error: " + ex.Message + ")";
                            Debug.WriteLine("[Error] " + errorMsg);
                            var msgDialog = new MessageDialog(errorMsg);
                            await msgDialog.ShowAsync();
                            return;
                        }
                        if (resolutions.Result == null)
                        {
                            String errorMsg = "SetSelectedCamera: Failed to GetVideoCaptureCapabilities (Result is null)";
                            Debug.WriteLine("[Error] " + errorMsg);
                            var msgDialog = new MessageDialog(errorMsg);
                            await msgDialog.ShowAsync();
                            return;
                        }
                        var uniqueRes = resolutions.Result.GroupBy(test => test.ResolutionDescription).Select(grp => grp.First()).ToList();
                        CaptureCapability defaultResolution = null;
                        foreach (var resolution in uniqueRes)
                        {
                            if (defaultResolution == null)
                            {
                                defaultResolution = resolution;
                            }
                            _allCapRes.Add(resolution.ResolutionDescription);
                            if ((resolution.Width == 640) && (resolution.Height == 480))
                            {
                                defaultResolution = resolution;
                            }
                        }
                        string selectedCapResItem = string.Empty;

                        if (!string.IsNullOrEmpty(selectedCapResItem) && _allCapRes.Contains(selectedCapResItem))
                        {
                            SelectedCapResItem = selectedCapResItem;
                        }
                        else
                        {
                            SelectedCapResItem = defaultResolution.ResolutionDescription;
                        }
                    });                 
                });
            }
        }

        private String _selectedCapResItem;

        /// <summary>
        /// The selected capture resolution.
        /// </summary>
        public String SelectedCapResItem
        {
            get { return _selectedCapResItem; }
            set
            {
                if (AllCapFPS == null)
                {
                    AllCapFPS = new Collection<CaptureCapability>();
                }
                else
                {
                    AllCapFPS.Clear();
                }
                var opCap = SelectedCamera.GetVideoCaptureCapabilities();
                opCap.AsTask().ContinueWith(caps =>
                {
                    var fpsList = from cap in caps.Result where cap.ResolutionDescription == value select cap;
                    var t = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            CaptureCapability defaultFPS = null;
                            uint selectedCapFPSFrameRate = 0;

                            foreach (var fps in fpsList)
                            {
                                if (selectedCapFPSFrameRate != 0 && fps.FrameRate == selectedCapFPSFrameRate)
                                {
                                    defaultFPS = fps;
                                }
                                AllCapFPS.Add(fps);
                                if (defaultFPS == null)
                                {
                                    defaultFPS = fps;
                                }
                            }
                            SelectedCapFPSItem = defaultFPS;
                        });
                });
                 _selectedCapResItem = value;
            }
        }

        private Collection<CaptureCapability> _allCapFPS;

        /// <summary>
        /// The list of all capture frame rates.
        /// </summary>
        private Collection<CaptureCapability> AllCapFPS
        {
            get
            {
                if (_allCapFPS == null)
                {
                    _allCapFPS = new Collection<CaptureCapability>();
                }
                return _allCapFPS;
            }
            set { _allCapFPS = value; }
        }

        private CaptureCapability _selectedCapFPSItem;
        /// <summary>
        /// The selected capture frame rate.
        /// </summary>
        private CaptureCapability SelectedCapFPSItem
        {
            get { return _selectedCapFPSItem; }
            set
            {
                if (_selectedCapFPSItem != value)
                {
                    _selectedCapFPSItem = value;
                    Conductor.Instance.VideoCaptureProfile = value;
                    Conductor.Instance.updatePreferredFrameFormat();
                }
            }
        }

        private Collection<MediaDevice> _microphones;

        /// <summary>
        /// The list of available microphones.
        /// </summary>
        private Collection<MediaDevice> Microphones
        {
            get
            {
                return _microphones;
            }
            set { _microphones = value; }
        }

        private MediaDevice _selectedMicrophone;

        /// <summary>
        /// The selected microphone.
        /// </summary>
        private MediaDevice SelectedMicrophone
        {
            get { return _selectedMicrophone; }
            set
            {
                if (Equals(_selectedMicrophone, value) && value != null)
                {
                    _selectedMicrophone = value;
                    Conductor.Instance.Media.SelectAudioCaptureDevice(_selectedMicrophone);
                }
            }
        }

        private Collection<MediaDevice> _audioPlayoutDevices;

        /// <summary>
        /// The list of available audio playout devices.
        /// </summary>
        private Collection<MediaDevice> AudioPlayoutDevices
        {
            get
            {
                return _audioPlayoutDevices;
            }
            set
            {
                _audioPlayoutDevices = value;
            }
        }

        private MediaDevice _selectedAudioPlayoutDevice;

        /// <summary>
        /// The selected audio playout device.
        /// </summary>
        private MediaDevice SelectedAudioPlayoutDevice
        {
            get { return _selectedAudioPlayoutDevice; }
            set
            {
                if (Equals(_selectedAudioPlayoutDevice, value) && value != null)
                {
                    _selectedAudioPlayoutDevice = value;
                    Conductor.Instance.Media.SelectAudioPlayoutDevice(_selectedAudioPlayoutDevice);
                }
            }
        }

        private Collection<IceServer> _iceServers;

        /// <summary>
        /// The list of Ice servers.
        /// </summary>
        private Collection<IceServer> IceServers
        {
            get { return _iceServers; }
            set { _iceServers = value; }
        }



        #endregion
#else
        #region Variables-Unity-Placeholder

        public String SelectedCapResItem;
        public Collection<String> AllCapRes;

        // More Placholder for the Getter and Setter of complex-variables in the functions-region

        #endregion
#endif
        #region Variables-Shared

        private int _peerId;

        private Collection<Peer> _peers;

        /// <summary>
        /// List of avaible peers
        /// </summary>
        private Collection<Peer> Peers
        {
            get
            {
                return _peers;
            }
            set
            {
                _peers = value;
            }
        }

        private string _ip = null, _host = null;

        /// <summary>
        /// The ip of the signalling server
        /// </summary>
        public String Ip
        {
            get { return _ip; }
            set { _ip = value; }
        }

        /// <summary>
        /// The port of the signalling server
        /// </summary>
        public String Port
        {
            get { return _host; }
            set { _host = value; }
        }

        public bool IsInit { private set { _isInit = value; } get { return _isInit; } }
        private bool _isInit = false;

        public String ErrMsg { private set { _errormsg = value; OnError?.Invoke(); } get { return _errormsg; } }
        private String _errormsg = "";

        public ErrorCode ErrCode { private set { _errorcode = value; } get { return _errorcode; } }
        private ErrorCode _errorcode = ErrorCode.NO_ERROR;

        public bool IsConnected { private set { _connected = value; } get { return _connected; } }
        private bool _connected = false;

        public bool IsConnecting { private set { _connecting = value; } get { return _connecting; }  }
        private bool _connecting = false;

        public bool IsDisconnecting { private set { _disconnecting = value; } get { return _disconnecting; }  }
        private bool _disconnecting = false;

        public bool IsReadyToDisconnect { private set { _isreadytodisconnect = value; } get { return _isreadytodisconnect; } }
        private bool _isreadytodisconnect = false;

        public bool IsReadyToConnect { private set { _isreadytoconnect = value; } get { return _isreadytoconnect; } }
        private bool _isreadytoconnect = false;

        public bool IsConnectedToPeer { private set { _isconnecttopeer = value; } get { return _isconnecttopeer; }  }
        private bool _isconnecttopeer = false;

        public bool HasAccess { private set { _access = value; } get { return _access; } }
        private bool _access = false;

        public bool CheckedForAccess { private set { _checkedForAccess = value; } get { return _checkedForAccess; } }
        private bool _checkedForAccess = false;
        #endregion

        #endregion

        #region Functions

#if NETFX_CORE

        #region Getter and Setter for Complex-Variables

        /// <summary>
        /// The list of available cameras.
        /// </summary>
        public Collection<UnityMediaDevice> GetCameras()
        {
            Collection<UnityMediaDevice> coll = new Collection<UnityMediaDevice>();
            foreach(MediaDevice m in Cameras)
            {
                coll.Add(new UnityMediaDevice{ Id = m.Id, Name = m.Name });
            }
            return coll;
        }

        /// <summary>
        /// The selected camera.
        /// </summary>
        public UnityMediaDevice GetSelectedCamera()
        {
            if (SelectedCamera == null) return null;
            return new UnityMediaDevice { Name = SelectedCamera.Name, Id = SelectedCamera.Id };
        }

        /// <summary>
        /// Set the selected camera.
        /// </summary>
        public bool SetSelectedCamera(UnityMediaDevice device)
        {
            MediaDevice mDevice = Cameras.FirstOrDefault(x => x.Id == device.Id);
            if (mDevice == null) return false;
            else
            {
                SelectedCamera = mDevice;
                return true;
            }
        }

        /// <summary>
        /// The list of available microphones.
        /// </summary>
        public Collection<UnityMediaDevice> GetMicrophones()
        {
            Collection<UnityMediaDevice> coll = new Collection<UnityMediaDevice>();
            foreach (MediaDevice m in Microphones)
            {
                coll.Add(new UnityMediaDevice { Id = m.Id, Name = m.Name });
            }
            return coll;
        }

        /// <summary>
        /// The selected microphone.
        /// </summary>
        public UnityMediaDevice GetSelectedMicrophone()
        {
            if (SelectedMicrophone == null) return null;
            return new UnityMediaDevice { Name = SelectedMicrophone.Name, Id = SelectedMicrophone.Id };
        }

        /// <summary>
        /// Set the selected microphone.
        /// </summary>
        public bool SetSelectedMicrophone(UnityMediaDevice device)
        {
            MediaDevice mDevice = Microphones.FirstOrDefault(x => x.Id == device.Id);
            if (mDevice == null) return false;
            else
            {
                SelectedMicrophone = mDevice;
                return true;
            }
        }

        /// <summary>
        /// The list of available audio playout devices.
        /// </summary>
        public Collection<UnityMediaDevice> GetAudioPlayoutDevices()
        {
            Collection<UnityMediaDevice> coll = new Collection<UnityMediaDevice>();
            foreach (MediaDevice m in AudioPlayoutDevices)
            {
                coll.Add(new UnityMediaDevice { Id = m.Id, Name = m.Name });
            }
            return coll;
        }

        /// <summary>
        /// The selected audio playout device.
        /// </summary>
        public UnityMediaDevice GetSelectedAudioPlayoutDevice()
        {
            if (SelectedAudioPlayoutDevice == null) return null;
            return new UnityMediaDevice { Name = SelectedAudioPlayoutDevice.Name, Id = SelectedAudioPlayoutDevice.Id };
        }

        /// <summary>
        /// Set the selected audio playout device.
        /// </summary>
        public bool SetSelectedAudioPlayoutDevice(UnityMediaDevice device)
        {
            MediaDevice mDevice = AudioPlayoutDevices.FirstOrDefault(x => x.Id == device.Id);
            if (mDevice == null) return false;
            else
            {
                SelectedAudioPlayoutDevice = mDevice;
                return true;
            }
        }

        /// <summary>
        /// The list of available peers.
        /// </summary>
        public Collection<Peer> GetPeers()
        {
            return Peers;
        }

        /// <summary>
        /// The list of available FPS.
        /// </summary>
        public Collection<UnityCaptureCapability> GetAllCapFPS()
        {
            Collection<UnityCaptureCapability> coll = new Collection<UnityCaptureCapability>();
            foreach(CaptureCapability cc in AllCapFPS)
            {
                coll.Add(new UnityCaptureCapability {
                    FrameRate = cc.FrameRate,
                    FrameRateDescription = cc.FrameRateDescription,
                    FullDescription = cc.FullDescription,
                    Height = cc.Height,
                    ResolutionDescription = cc.ResolutionDescription,
                    Width = cc.Width
                });
            }
            return coll;
        }

        /// <summary>
        /// The selected FPS.
        /// </summary>
        public UnityCaptureCapability GetSelectedCapFPS()
        {
            if (SelectedCapFPSItem == null) return null;
            else return new UnityCaptureCapability
            {
                FrameRate = SelectedCapFPSItem.FrameRate,
                FrameRateDescription = SelectedCapFPSItem.FrameRateDescription,
                FullDescription = SelectedCapFPSItem.FullDescription,
                Height = SelectedCapFPSItem.Height,
                ResolutionDescription = SelectedCapFPSItem.ResolutionDescription,
                Width = SelectedCapFPSItem.Width
            };
        }

        /// <summary>
        /// Set the selected FPS.
        /// </summary>
        public bool SetSelectedCapFPS(UnityCaptureCapability ucc)
        {
            CaptureCapability cc = AllCapFPS.FirstOrDefault(x => CompareCapFPS(x, ucc));
            if (cc == null) return false;
            else
            {
                SelectedCapFPSItem = cc;
                return true;
            }
        }

        private bool CompareCapFPS(CaptureCapability cc, UnityCaptureCapability ucc)
        {
            return (ucc.FrameRate.Equals(cc.FrameRate) &&
                ucc.FrameRateDescription.Equals(cc.FrameRateDescription) &&
                ucc.FullDescription.Equals(cc.FullDescription) &&
                ucc.Height.Equals(cc.Height) &&
                ucc.ResolutionDescription.Equals(cc.ResolutionDescription) &&
                ucc.Width.Equals(cc.Width));
        }

        #endregion

        #region Functions-Unity
        // Funktionen die Platzhalter fuer Unity benoetigen

        /// <summary>
        /// The Constructor for the WebRTC-Handler. Manages currently only one PeerConection at one time. The constructor will ask for permisson asynchron. 
        /// If permisson is granted it will also initialize the WebRTC. You can't initialize or connect manually unitl the check for permission is finished.
        /// Check the CheckedForAccess and Access properties.
        /// Please notice that also connection events are handled async.
        /// </summary>
        /// <param name="ip">The ip of the signalling-server</param>
        /// <param name="port">The port of the signalling server</param>
        /// <param name="sig">Coustom signaller</param>
        public UWebRTC(string ip = null, string port = null, ISignaler sig = null)
        {
            Ip = ip;
            Port = port;
            if(sig != null)
            {
                Conductor.useDefaultSignaller = false;
                Conductor.Signaller = sig;
            }
            SetDefaultHandler();
            WebRTC.RequestAccessForMediaCapture().AsTask().ContinueWith(antecedent =>
            {
                RunOnUiThread(() =>
                {
                    CheckedForAccess = true;
                    if (antecedent.Result)
                    {
                        HasAccess = true;
                        Initialize();
                    }
                    else
                    {
                        Error("Failed to obtain access to multimedia devices!", ErrorCode.ACCESS_NOT_GRANTED);
                    }
                });
            });
        }

        /// <summary>
        /// Initialize the WebRTC and loads avaible devices and sets default iceserver
        /// </summary>
        /// <returns>Returns true if successfull</returns>
        public bool Initialize()
        {            
            if(Dispatcher == null)
            {
                Error("No Dispatcher found. Did you set it in the App?", ErrorCode.NO_DISPATCHER);
                return false;
            }
            if (!HasAccess)
            {
                string err = "No media-device-access.";
                ErrorCode c = ErrorCode.ACCESS_NOT_GRANTED;
                if (!CheckedForAccess)
                {
                    err += " Check for access not yet finished.";
                    c = ErrorCode.ACCESS_NOT_CHECKED;
                }
                Error(err, c);
                return false;
            }
            if (!IsInit)
            {
                WebRTC.Initialize(Dispatcher);

                // Get information of cameras attached to the device
                Cameras = new Collection<MediaDevice>();
                string savedVideoRecordingDeviceId = null;
                foreach (MediaDevice videoCaptureDevice in Conductor.Instance.Media.GetVideoCaptureDevices())
                {
                    if (savedVideoRecordingDeviceId != null && savedVideoRecordingDeviceId == videoCaptureDevice.Id)
                    {
                        SelectedCamera = videoCaptureDevice;
                    }
                    Cameras.Add(videoCaptureDevice);
                }
                if (SelectedCamera == null && Cameras.Count > 0)
                {
                    SelectedCamera = Cameras.First();
                }

                // Get information of microphones attached to the device
                Microphones = new Collection<MediaDevice>();
                string savedAudioRecordingDeviceId = null;
                foreach (MediaDevice audioCaptureDevice in Conductor.Instance.Media.GetAudioCaptureDevices())
                {
                    if (savedAudioRecordingDeviceId != null && savedAudioRecordingDeviceId == audioCaptureDevice.Id)
                    {
                        SelectedMicrophone = audioCaptureDevice;
                    }
                    Microphones.Add(audioCaptureDevice);
                }
                if (SelectedMicrophone == null && Microphones.Count > 0)
                {
                    SelectedMicrophone = Microphones.First();
                }

                // Get information of speakers attached to the device
                AudioPlayoutDevices = new Collection<MediaDevice>();
                string savedAudioPlayoutDeviceId = null;
                foreach (MediaDevice audioPlayoutDevice in Conductor.Instance.Media.GetAudioPlayoutDevices())
                {
                    if (savedAudioPlayoutDeviceId != null && savedAudioPlayoutDeviceId == audioPlayoutDevice.Id)
                    {
                        SelectedAudioPlayoutDevice = audioPlayoutDevice;
                    }

                    AudioPlayoutDevices.Add(audioPlayoutDevice);
                }
                if (SelectedAudioPlayoutDevice == null && AudioPlayoutDevices.Count > 0)
                {
                    SelectedAudioPlayoutDevice = AudioPlayoutDevices.First();
                }

                Conductor.Instance.Media.OnMediaDevicesChanged += OnMediaDevicesChanged;

                IceServers = new Collection<IceServer>();
                IceServers.Add(new IceServer("stun.l.google.com", "19302", IceServer.ServerType.STUN));
                IceServers.Add(new IceServer("stun1.l.google.com", "19302", IceServer.ServerType.STUN));
                IceServers.Add(new IceServer("stun2.l.google.com", "19302", IceServer.ServerType.STUN));
                IceServers.Add(new IceServer("stun3.l.google.com", "19302", IceServer.ServerType.STUN));
                IceServers.Add(new IceServer("stun4.l.google.com", "19302", IceServer.ServerType.STUN));
                Conductor.Instance.ConfigureIceServers(IceServers);

                IsInit = true;
                //RunOnUiThread(async () =>
                //{
                //    MediaCapture mc = new MediaCapture();
                //    await mc.InitializeAsync();
                //    IRandomAccessStream RandomAccStream = new InMemoryRandomAccessStream();
                //    IVideoEffectDefinition ved = new VideoMRCSettings();
                //    await mc.AddVideoEffectAsync(ved, MediaStreamType.VideoRecord);
                //    MediaEncodingProfile vidFormat = MediaEncodingProfile.CreateMp4(VideoEncodingQuality.HD720p);
                //    await mc.StartRecordToStreamAsync(vidFormat, RandomAccStream);
                //    Debug.WriteLine("StartStream");
                //});
            }
            
            return IsInit;
        }

        /// <summary>
        /// Handle media devices change event triggered by WebRTC.
        /// </summary>
        /// <param name="mediaType">The type of devices changed</param>
        private void OnMediaDevicesChanged(MediaDeviceType mediaType)
        {
            switch (mediaType)
            {
                case MediaDeviceType.MediaDeviceType_VideoCapture:
                    RefreshVideoCaptureDevices();
                    break;
                case MediaDeviceType.MediaDeviceType_AudioCapture:
                    RefreshAudioCaptureDevices();
                    break;
                case MediaDeviceType.MediaDeviceType_AudioPlayout:
                    RefreshAudioPlayoutDevices();
                    break;
            }
        }

        /// <summary>
        /// Refresh video capture devices list.
        /// </summary>
        private void RefreshVideoCaptureDevices()
        {
            var videoCaptureDevices = Conductor.Instance.Media.GetVideoCaptureDevices();
            RunOnUiThread(() => {
                Collection<MediaDevice> videoCaptureDevicesToRemove = new Collection<MediaDevice>();
                foreach (MediaDevice videoCaptureDevice in Cameras)
                {
                    if (videoCaptureDevices.FirstOrDefault(x => x.Id == videoCaptureDevice.Id) == null)
                    {
                        videoCaptureDevicesToRemove.Add(videoCaptureDevice);
                    }
                }
                foreach (MediaDevice removedVideoCaptureDevices in videoCaptureDevicesToRemove)
                {
                    if (SelectedCamera.Id == removedVideoCaptureDevices.Id)
                    {
                        SelectedCamera = null;
                    }
                    Cameras.Remove(removedVideoCaptureDevices);
                }
                foreach (MediaDevice videoCaptureDevice in videoCaptureDevices)
                {
                    if (Cameras.FirstOrDefault(x => x.Id == videoCaptureDevice.Id) == null)
                    {
                        Cameras.Add(videoCaptureDevice);
                    }
                }

                if (SelectedCamera == null)
                {
                    SelectedCamera = Cameras.FirstOrDefault();
                }
            });
        }

        /// <summary>
        /// Refresh audio capture devices list.
        /// </summary>
        private void RefreshAudioCaptureDevices()
        {
            var audioCaptureDevices = Conductor.Instance.Media.GetAudioCaptureDevices();
            RunOnUiThread(() => {
                var SelectedMicrophoneId = SelectedMicrophone != null ? SelectedMicrophone.Id : null;
                SelectedMicrophone = null;
                Microphones.Clear();
                foreach (MediaDevice audioCaptureDevice in audioCaptureDevices)
                {
                    Microphones.Add(audioCaptureDevice);
                    if (audioCaptureDevice.Id == SelectedMicrophoneId)
                    {
                        SelectedMicrophone = Microphones.Last();
                    }
                }
                if (SelectedMicrophone == null)
                {
                    SelectedMicrophone = Microphones.FirstOrDefault();
                }

                if (SelectedMicrophone == null)
                {
                    SelectedMicrophone = Microphones.FirstOrDefault();
                }
            });
        }

        /// <summary>
        /// Refresh audio playout devices list.
        /// </summary>
        private void RefreshAudioPlayoutDevices()
        {
            var audioPlayoutDevices = Conductor.Instance.Media.GetAudioPlayoutDevices();
            RunOnUiThread(() => {
                var SelectedPlayoutDeviceId = SelectedAudioPlayoutDevice != null ? SelectedAudioPlayoutDevice.Id : null;
                SelectedAudioPlayoutDevice = null;
                AudioPlayoutDevices.Clear();
                foreach (MediaDevice audioPlayoutDevice in audioPlayoutDevices)
                {
                    AudioPlayoutDevices.Add(audioPlayoutDevice);
                    if (audioPlayoutDevice.Id == SelectedPlayoutDeviceId)
                    {
                        SelectedAudioPlayoutDevice = audioPlayoutDevice;
                    }
                }
                if (SelectedAudioPlayoutDevice == null)
                {
                    SelectedAudioPlayoutDevice = AudioPlayoutDevices.FirstOrDefault();
                }
            });
        }

        /// <summary>
        /// Connects to the signalling-server
        /// </summary>
        public void ConnectToServer()
        {
            if (Initialize() && !IsConnected && !IsConnecting && !IsDisconnecting)
            {
                IsConnecting = true;
                Conductor.Instance.StartLogin(Ip, Port);
            }
            
        }

        /// <summary>
        /// Disconnects from the signalling-server
        /// </summary>
        public void DisconnectFromServer()
        {
            if (!IsConnected || IsDisconnecting) return;
            new Task(() =>
            {
                IsDisconnecting = true;
                var task = Conductor.Instance.DisconnectFromServer();
            }).Start();
        }

        /// <summary>
        /// Connects to a peer
        /// </summary>
        /// <param name="peerId">The id of the peer</param>
        public void ConnectToPeer(int peerId)
        {
            if(IsConnectedToPeer)
            {
                Error("Only one connection at one time. Currently connected with " + Peers.FirstOrDefault(x => x.Id == _peerId).ToString(), ErrorCode.ONLY_ONE_PEERCONNECTION);
                return;
            }
            new Task(() =>
            {
                Conductor.Instance.ConnectToPeer(peerId);
            }).Start();
        }

        /// <summary>
        /// Disconnects from the connected peer
        /// </summary>
        public void DisconnectFromPeer()
        {
            if (!IsConnectedToPeer) return;
            new Task(() =>
            {
                var task = Conductor.Instance.DisconnectFromPeer();
            }).Start();
        }

        /// <summary>
        /// Mutes or unmutes the mircophone
        /// </summary>
        /// <param name="mute">true if mircophone should be muted</param>
        public void MuteMicrophone(bool mute)
        {
            if (mute)
            {
                Conductor.Instance.MuteMicrophone();
            }
            else
            {
                Conductor.Instance.UnmuteMicrophone();
            }
        }

        /// <summary>
        /// Disables or enables the videostream
        /// </summary>
        /// <param name="disable">true it the viedostream should be disabled</param>
        public void DisableVideo(bool disable)
        {
            if (disable)
            {
                Conductor.Instance.DisableLocalVideoStream();
            }
            else
            {
                Conductor.Instance.EnableLocalVideoStream();
            }
        }

        //Private Funktione die nicht in Unity laufen (--> keine Platzhalter)

        /// <summary>
        /// Handler for signaller-event
        /// </summary>
        private void SignedIn()
        {
            RunOnUiThread(() =>
            {
                IsConnecting = false;
                IsConnected = true;
            });
        }

        /// <summary>
        /// Handler for signaller-event
        /// </summary>
        private void Disconnect()
        {
            RunOnUiThread(() =>
            {
                IsDisconnecting = false;
                IsConnected = false;
                if (Peers != null)
                {
                    Peers.Clear();
                }
            });
        }

        /// <summary>
        /// Handler for signaller-event
        /// </summary>
        private void PeerConnected(int peer_id, string peername)
        {
            RunOnUiThread(() =>
            {
                if (Peers == null)
                    Peers = new Collection<Peer>();
                Peers.Add(new Peer { Id = peer_id, Name = peername });
            });
        }

        /// <summary>
        /// Handler for signaller-event
        /// </summary>
        private void PeerDisconnected(int peer_id)
        {
            RunOnUiThread(() =>
            {
                if (Peers != null)
                {
                    var peerToRemove = Peers.FirstOrDefault(p => p.Id == peer_id);
                    if (peerToRemove != null)
                        Peers.Remove(peerToRemove);
                }
            });
        }

        /// <summary>
        /// Handler for signaller-event
        /// </summary>
        //private void PeerHangup(int peer_id)
        //{
        //    RunOnUiThread(() =>
        //    {

        //    });
        //}

        /// <summary>
        /// Handler for signaller-event
        /// </summary>
        //private void MessageFromPeer(int peer_id, string message)
        //{
        //    RunOnUiThread(() =>
        //    {

        //    });
        //}

        /// <summary>
        /// Handler for signaller-event
        /// </summary>
        private void ServerConnectionFailure()
        {
            RunOnUiThread(() =>
            {
                IsConnecting = false;
                Error("Unable to connect to Server", ErrorCode.CANNOT_CONNECT_TO_SERVER);
            });
        }

        protected void RunOnUiThread(Action fn)
        {
            var asyncOp = _dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(fn));
        }

        /// <summary>
        /// Adds an IceServer
        /// </summary>
        public void AddIceServer( )
        {
            throw new NotImplementedException("IceServerClass has not yet a placeholder for Unity");
            //IceServers.Add(ice);
            //Conductor.Instance.ConfigureIceServers(IceServers);
        }

        /// <summary>
        /// Removes an IceServer
        /// </summary>
        public void RemoveIceServer( )
        {
            throw new NotImplementedException("IceServerClass has not yet a placeholder for Unity");
            //IceServers.Remove(ice);
            //Conductor.Instance.ConfigureIceServers(IceServers);
        }


        /// <summary>
        /// Sets the handler for the signaller-events
        /// </summary>
        private void SetDefaultHandler()
        {
            if(Conductor.Signaller == null)
            {
                Conductor c = Conductor.Instance; // Init conductor-singleton
            }
            Conductor.Signaller.OnSignedIn += SignedIn;
            Conductor.Signaller.OnDisconnected += Disconnect;
            Conductor.Signaller.OnPeerConnected += PeerConnected;
            Conductor.Signaller.OnPeerDisconnected += PeerDisconnected;
            //Conductor.Signaller.OnPeerHangup += PeerHangup;
            //Conductor.Signaller.OnMessageFromPeer += MessageFromPeer;
            Conductor.Signaller.OnServerConnectionFailure += ServerConnectionFailure;

            Conductor.Instance.OnPeerConnectionCreated += Conductor_OnPeerConnectionCreated;
            Conductor.Instance.OnPeerConnectionClosed += Conductor_OnPeerConnectionClosed;
            Conductor.Instance.OnReadyToConnect += Conductor_OnReadyToConnect;
        }

        /// <summary>
        /// Handler for conductor-event
        /// </summary>
        private void Conductor_OnPeerConnectionCreated(int peerId)
        {
            OnPeerConnectionCreated?.Invoke();
            RunOnUiThread(() =>
            {
                IsReadyToConnect = false;
                IsConnectedToPeer = true;
                IsReadyToDisconnect = true;
                _peerId = peerId;
            });
        }

        /// <summary>
        /// Handler for conductor-event
        /// </summary>
        private void Conductor_OnPeerConnectionClosed()
        {
            OnPeerConnectionClosed?.Invoke();
            RunOnUiThread(() =>
            {
                IsConnectedToPeer = false;
                IsReadyToDisconnect = false;
                _peerId = -1;
            });
        }

        /// <summary>
        /// Handler for conductor-event
        /// </summary>
        private void Conductor_OnReadyToConnect()
        {
            OnReadyToConnect?.Invoke();
            RunOnUiThread(() =>
            {
                IsReadyToConnect = true;
            });
        }

        private void Error(string msg, ErrorCode code)
        {
            // Set code before msg, since msg fires the OnError-Event
            ErrCode = code;
            ErrMsg = msg;
        }

        private void Error(string msg)
        {
            // Set code before msg, since msg fires the OnError-Event
            ErrCode = ErrorCode.UNKNOWN_ERROR;
            ErrMsg = msg;
        }
        #endregion

#else

        #region Functions-Placeholder
        // Unity- Placeholder

        /// <summary>
        /// The Constructor for the WebRTC-Handler. Manages currently only one PeerConection at one time.
        /// </summary>
        /// <param name="ip">The ip of the signalling-server</param>
        /// <param name="port">The port of the signalling server</param>
        /// <param name="sig">Coustom signaller</param>
        public UWebRTC(string ip=null, string port=null, ISignaler sig = null)
        {

        }

        /// <summary>
        /// Initialize the WebRTC and loads avaible devices and sets default iceserver. Returns always false in Unity-Editor.
        /// </summary>
        /// <returns>Returns true if successfull</returns>
        public bool Initialize()
        {
            return false;
        }

        /// <summary>
        /// Connects to the signalling-server
        /// </summary>
        public void ConnectToServer()
        {

        }

        /// <summary>
        /// Disconnects from the server
        /// </summary>
        public void DisconnectFromServer()
        {

        }

        /// <summary>
        /// Connects to a peer
        /// </summary>
        /// <param name="peerId">The id of the peer</param>
        public void ConnectToPeer(int peerId)
        {

        }

        /// <summary>
        /// Disconnects from the connected peer
        /// </summary>
        public void DisconnectFromPeer()
        {

        }

        /// <summary>
        /// Mutes or unmutes the mircophone
        /// </summary>
        /// <param name="mute">true if mircophone should be muted</param>
        public void MuteMicrophone(bool mute)
        {

        }

        /// <summary>
        /// Disables or enables the videostream
        /// </summary>
        /// <param name="disable">true it the viedostream should be disabled</param>
        public void DisableVideo(bool disable)
        {

        }

        /// <summary>
        /// Adds an IceServer
        /// </summary>
        public void AddIceServer()
        {

        }

        /// <summary>
        /// Removes an IceServer
        /// </summary>
        public void RemoveIceServer()
        {
      
        }

        // Getter and Setter Placeholder
        /// <summary>
        /// The list of available cameras. Returns in Unity-Editor always null.
        /// </summary>
        public Collection<UnityMediaDevice> GetCameras()
        {
            return null;
        }

        /// <summary>
        /// The selected camera. Returns in Unity-Editor always null.
        /// </summary>
        public UnityMediaDevice GetSelectedCamera()
        {
            return null;
        }

        /// <summary>
        /// Set the selected camera. Returns in Unity-Editor always false.
        /// </summary>
        public bool SetSelectedCamera(UnityMediaDevice device)
        {
            return false;
        }

        /// <summary>
        /// The list of available microphones. Returns in Unity-Editor always null.
        /// </summary>
        public Collection<UnityMediaDevice> GetMicrophones()
        {
            return null;
        }

        /// <summary>
        /// The selected microphone. Returns in Unity-Editor always null.
        /// </summary>
        public UnityMediaDevice GetSelectedMicrophone()
        {
            return null;
        }

        /// <summary>
        /// Set the selected microphone. Returns in Unity-Editor always false.
        /// </summary>
        public bool SetSelectedMicrophone(UnityMediaDevice device)
        {
            return false;
        }

        /// <summary>
        /// The list of available audio playout devices. Returns in Unity-Editor always null.
        /// </summary>
        public Collection<UnityMediaDevice> GetAudioPlayoutDevices()
        {
            return null;
        }

        /// <summary>
        /// The selected audio playout device. Returns in Unity-Editor always null.
        /// </summary>
        public UnityMediaDevice GetSelectedAudioPlayoutDevice()
        {
            return null;
        }

        /// <summary>
        /// Set the selected audio playout device. Returns in Unity-Editor always false.
        /// </summary>
        public bool SetSelectedAudioPlayoutDevice(UnityMediaDevice device)
        {
            return false;
        }

        /// <summary>
        /// The list of available peers. Returns in Unity-Editor always null.
        /// </summary>
        public Collection<Peer> GetPeers()
        {
            return null;
        }

        /// <summary>
        /// The list of available FPS. Returns in Unity-Editor always null.
        /// </summary>
        public Collection<UnityCaptureCapability> GetAllCapFPS()
        {
            return null;
        }

        /// <summary>
        /// The selected FPS. Returns in Unity-Editor always null.
        /// </summary>
        public UnityCaptureCapability GetSelectedCapFPS()
        {
            return null;
        }

        /// <summary>
        /// Set the selected FPS. Returns in Unity-Editor always false.
        /// </summary>
        public bool SetSelectedCapFPS(UnityCaptureCapability ucc)
        {
            return false;
        }
        #endregion

#endif

        public void ResetError()
        {
            _errormsg = ""; // Setting ErrMsg would trigger the OnError-Event
            ErrCode = ErrorCode.NO_ERROR;
        }
        #endregion
    }

    /// <summary>
    /// Avaible Peer.
    /// </summary>
    public class Peer
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Id + ": " + Name;
        }
    }

    /// <summary>
    /// Unity-Placeholder for a mediaDevice (camera, microphone, ...)
    /// </summary>
    public class UnityMediaDevice
    {
        public string Name { get; set; }
        public string Id { get; set; }

        public override string ToString()
        {
            return Id + ": " + Name;
        }
    }

    /// <summary>
    /// Unity-Placeholder for the capturecapability of a camera
    /// </summary>
    public class UnityCaptureCapability
    {
        public string FrameRateDescription;
        public string FullDescription;
        public string ResolutionDescription;
        public uint Height;
        public uint Width;
        public uint FrameRate;
    }

    public enum ErrorCode
    {
        NO_ERROR,
        NO_DISPATCHER,
        ACCESS_NOT_CHECKED,
        ACCESS_NOT_GRANTED,
        NOT_INITIALIZESED,
        CANNOT_CONNECT_TO_SERVER,
        ONLY_ONE_PEERCONNECTION,
        UNKNOWN_ERROR
    }

#if NETFX_CORE

    public class VideoMRCSettings : IVideoEffectDefinition
    {
        public string ActivatableClassId
        {
            get
            {
                return "Windows.Media.MixedRealityCapture.MixedRealityCaptureVideoEffect";
            }
        }

        public IPropertySet Properties
        {
            get
            {
                return _Properties;
            }
        }

        private IPropertySet _Properties;
        public VideoMRCSettings()
        {
            _Properties = (IPropertySet)new PropertySet();
            _Properties.Add("HologramCompositionEnabled", true);
            _Properties.Add("RecordingIndicatorEnabled", true);
            _Properties.Add("VideoStabilizationEnabled", false);
            _Properties.Add("VideoStabilizationBufferLength", 0);
            _Properties.Add("GlobalOpacityCoefficient", 0.9f);
        }
    }

#else
    /// <summary>
    /// Placeholder Class for WebRTC.Media.MediaDevice to use in Unity
    /// </summary>
    public class MediaDevice
        {
            public string Name { get; set; }
            public int Id { get; set; }
        }
#endif
}
