﻿#if NETFX_CORE
using System.Threading.Tasks;
using Windows.Data.Json;
#endif

namespace UnityWebRTC.Interface
{
    public delegate void SignedInDelegate();
    public delegate void DisconnectedDelegate();
    public delegate void PeerConnectedDelegate(int id, string name);
    public delegate void PeerDisonnectedDelegate(int peer_id);
    public delegate void PeerHangupDelegate(int peer_id);
    public delegate void MessageFromPeerDelegate(int peer_id, string message);
    public delegate void MessageSentDelegate(int err);
    public delegate void ServerConnectionFailureDelegate();

    /// <summary>
    /// The Interface for a Signaller-Class that the Conductor awaits.
    /// Most functions need a Unity-Placholder: 
    /// #if NETFX_CORE
    ///     real function-implementation
    /// #else
    ///     Unity-Placholder
    /// #endif
    /// </summary>
    public interface ISignaler
    {
        // Connection events
        event SignedInDelegate OnSignedIn;
        event DisconnectedDelegate OnDisconnected;
        event PeerConnectedDelegate OnPeerConnected;
        event PeerDisonnectedDelegate OnPeerDisconnected;
        event PeerHangupDelegate OnPeerHangup;
        event MessageFromPeerDelegate OnMessageFromPeer;
        event ServerConnectionFailureDelegate OnServerConnectionFailure;

        /// <summary>
        /// Checks if connected to the server.
        /// </summary>
        /// <returns>True if connected to the server.</returns>
        bool IsConnected();

#if NETFX_CORE
        /// <summary>
        /// Connects to the server.
        /// </summary>
        /// <param name="server">Host name/IP.</param>
        /// <param name="port">Port to connect.</param>
        /// <param name="client_name">Client name.</param>
        /// <param name="pwd">Password</param>
        Task Connect(string server, string port, string client_name, string pwd = null);

        /// <summary>
        /// Disconnects the user from the server.
        /// </summary>
        /// <returns>True if the user is disconnected from the server.</returns>
        Task<bool> SignOut();

        /// <summary>
        /// Sends a message to a peer.
        /// </summary>
        /// <param name="peerId">ID of the peer to send a message to.</param>
        /// <param name="message">Message to send.</param>
        /// <returns>True if the message was sent.</returns>
        Task<bool> SendToPeer(int peerId, string message);

        /// <summary>
        /// Sends a message to a peer.
        /// </summary>
        /// <param name="peerId">ID of the peer to send a message to.</param>
        /// <param name="json">The json message.</param>
        /// <returns>True if the message is sent.</returns>
        Task<bool> SendToPeer(int peerId, IJsonValue json);
#else
         /// <summary>
        /// Connects to the server. !!This is only the Unity-Placholder.!!
        /// </summary>
        /// <param name="server">Host name/IP.</param>
        /// <param name="port">Port to connect.</param>
        /// <param name="client_name">Client name.</param>
        /// <param name="pwd">Password</param>
        void Connect(string server, string port, string client_name, string pwd = null);

        /// <summary>
        /// Disconnects the user from the server. !!This is only the Unity-Placholder.!!
        /// </summary>
        /// <returns>True if the user is disconnected from the server.</returns>
        bool SignOut();

        /// <summary>
        /// Sends a message to a peer. !!This is only the Unity-Placholder.!!
        /// </summary>
        /// <param name="peerId">ID of the peer to send a message to.</param>
        /// <param name="message">Message to send.</param>
        /// <returns>True if the message was sent.</returns>
        bool SendToPeer(int peerId, string message);

        /// <summary>
        /// Sends a message to a peer. !!This is only the Unity-Placholder.!!
        /// </summary>
        /// <param name="peerId">ID of the peer to send a message to.</param>
        /// <param name="json">The json message.</param>
        /// <returns>True if the message is sent.</returns>
        bool SendToPeer(int peerId, IJsonValue json);
#endif
    }

#if !NETFX_CORE
    /// <summary>
    /// Empty Class. Used as Unity-Placeholder.
    /// Use Windows.Data.Json outside Unity.
    /// </summary>
    public class IJsonValue
    {

    }
#endif
}
